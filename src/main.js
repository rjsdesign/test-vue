import { createApp } from "vue";
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from "./App.vue";
import "./index.css";
import router from "./router";
createApp(App).use(VueAxios, axios);
createApp(App).use(router).mount("#app");
