import { createWebHistory, createRouter } from "vue-router";
import Home from "./views/Home.vue";
import Category from "./views/id/Category.vue";
import Product from "./views/id/Product.vue";


const history = createWebHistory();
const routes = [
  { path: "/", component: Home },
  {path: "/category/:id_category",props:true, component: Category},
  {path: "/product/:id_product",props:true, component: Product}

];
const router = createRouter({ history, routes });
export default router;
